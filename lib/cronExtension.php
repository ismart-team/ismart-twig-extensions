<?php
class CronExtension extends \Twig_Extension {

    public function getFilters( ) {
        return array(
            'cron' => new \Twig_Filter_Method( $this, 'parse', array(
                'needs_environment' => true,
                'needs_context' => true,
                'is_safe' => array(
                    'evaluate' => true
                )
            ))
        );
    }

    public function parse( \Twig_Environment $environment, $context, $obj ) {
        $now = date("m/d/Y H:i");
        $nowDate = new DateTime($now);
        $newArr = array();

        foreach ($obj as $key => $value) {
            $deadline = new DateTime($key);
            if ($deadline > $nowDate) {
                $newArr[$key] = $value;
            }
        }
        ksort($newArr);
        return current($newArr);
    }

    public function getName( ) {
        return 'cron';
    }
}
